<?php
/**
 * MailSender file
 *
 * @author Sergey Malyshev <malyshev.php@gmail.com>
 */

require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'phpmailer'.DIRECTORY_SEPARATOR.'class.phpmailer.php');

/**
 * MailSender class
 * MailSender may be used in the following ways:
 * <pre>
 *			  $mail = Yii::app()->mail->createMail('signup/confirm',
 *				   array( //view data
 *					   'user'=>'User Name',
 *				   )
 *			   )->send('malyshev.php@gmail.com', 'Sergey Malyshev');
 *
 *			   $mail = Yii::app()->mail->createMail('signup/confirm',
 *				   array( //view data
 *					   'user'=>'User Name',
 *				   )
 *				)->send(array(
 *				   'malyshev.php@gmail.com',
 *				   'recipient@example.com'
 *			   ));
 *
 *			   $mail = Yii::app()->mail->createMail('signup/confirm',
 *				   array( //view data
 *					   'user'=>'User Name',
 *				   )
 *			   )->send(array(
 *				   array('malyshev.php@gmail.com'),
 *				   array('recipient@example.com'),
 *			   ));
 *
 *			   $mail = Yii::app()->mail->createMail('signup/confirm',
 *				   array( //view data
 *					   'user'=>'User Name',
 *				   )
 *			   )->send(array(
 *				   array('malyshev.php@gmail.com', 'Sergey Malyshev'),
 *				   array('recipient@example.com', 'Recipient Name'),
 *			   ));
 * </pre>
 *
 * @author Sergey Malyshev <malyshev.php@gmail.com>
 */


class MailSender extends CBaseController
{

	private $_mailer;

	public $behaviors = array(

	);

	public $debug = false;

	public $layout = 'email';

	public function __construct()
	{
		$this->_mailer = new PHPMailer;
		$this->attachBehaviors($this->behaviors);
	}

	public function __call($method, $params)
	{
		if(method_exists($this->_mailer,$method))
			return call_user_func_array(array($this->_mailer, $method), $params);

		return parent::__call($method, $params);
	}

	public function __set($name,$value)
	{
		$setter = 'set' . $name;
		if(method_exists($this->_mailer,$setter))
			return $this->_mailer->$setter($value);
		else if(method_exists($this->_mailer,$name))
			return $this->_mailer->$name($value);
		else if(property_exists($this->_mailer, $name))
			return $this->_mailer->$name = $value;

		return parent::__set($name,$value);
	}

	public function __get($name)
	{
	   if(property_exists($this->_mailer, $name))
			return $this->_mailer->$name;

	   return parent::__get($name);
	}

	public function init()
	{
	}

	/**
	 * Display email body
	 */
	public function display()
	{
		echo $this->_mailer->Body;
	}

	/**
	 * Send mail
	 * @param string $address recipient or recipients list
	 * @param string $name recipient name
	 * @return mixed 
	 */
	public function send($address, $name = '')
	{
		if($this->debug && ($support = Yii::app()->getParams()->adminEmail))
		{
			$this->bindAddress($support);
		}
		else
			$this->bindAddress($address, $name);

		$result =  $this->_mailer->Send();
		if($result && !$this->IsError())
		{
			$this->reset();
			return $result;
		}

		return $this->_mailer->ErrorInfo;
	}

	/**
	 * Cleanup mail transport parameters, use for destroy previous email
	 */
	public function reset()
	{
		$this->_mailer->ClearAddresses();
		$this->_mailer->ClearCCs();
		$this->_mailer->ClearBCCs();
		$this->_mailer->ClearReplyTos();
		$this->_mailer->ClearAllRecipients();
		$this->_mailer->ClearAttachments();
	}

	/**
	 * Set recipient email
	 * @param mixed $address
	 * @param string $name
	 */
	public function bindAddress($address, $name = '')
	{
		if(is_array($address))
		{
			while($recipient = array_shift($address))
			{
				if(is_array($recipient))
				{
					$n = '';
					if(sizeof($recipient) > 1)
						list($r, $n) = $recipient;
					else
						$r = array_shift($recipient);
					$this->_mailer->AddAddress($r, $n);
				}
				else
				{
					$this->_mailer->AddAddress($recipient);
				}
			}
		}
		else
			$this->_mailer->AddAddress($address, $name);
	}

	/**
	 * @param array $data bind parameters to object
	 */
	public function bindParams($data = array())
	{
		foreach($data as $k=>$v)
		{
			try
			{
				$this->$k = $v;
			}
			catch (Exception $e)
			{

			}
		}
	}

	/**
	 * Create mail from view file
	 * @param string view file
	 * @param array data to be extracted and made available to the view file
	 * @param boolean whether the mail is HTML
	 * @return self
	 */
	public function createMail($view,$data=array(),$isHTML=true)
	{
		$this->IsHTML($isHTML);
		$this->bindParams($data);
		$this->render($view,$data);
		return $this;
	}
	/**
	 * Create mail from html text
	 * @param string body
	 * @param array data to be extracted and made available to the view file
	 * @param boolean whether the mail is HTML
	 * @return self
	 */
	public function createMailText($body,$data=array(),$isHTML=true)
	{
		$this->IsHTML($isHTML);
		$this->bindParams($data);
//		$this->render($view,$data);
		$this->Body = $body;
		return $this;
	}

	/**
	 * Render email body using view file
	 * 
	 * @param string $viewFile view file path
	 * @param array $data data to be extracted and made available to the view
	 * @param boolean $return whether the rendering result should be returned instead of being echoed
	 * @return string the rendering result. Null if the rendering result is not required.
	 * @throws CException if the view file does not exist
	 */
	public function render($view, $data=null, $isHTML = true, $return=false)
	{
		if(($viewFile=$this->getViewFile($view))!==false)
		{
			$altBody = substr($viewFile, 0, strlen($viewFile)-4) . '.alt' . substr($viewFile, -4);

			if(!$return)
			{
				$this->Body = $this->renderFile($viewFile,$data,true);

				if(($layoutFile=$this->getLayoutFile($this->layout))!==false)
				{
					$this->_mailer->Body = $this->renderFile($layoutFile,array('content'=>$this->_mailer->Body),true);
				}

				if(file_exists($altBody) && is_file($altBody))
				{
					$this->AltBody = strip_tags( $this->renderFile($altBody,$data,true));
				}

				return true;
			}
			else if($return && $isHTML)
			{
				return $this->renderFile($viewFile,$data,true);
			}
			else if($return && !$isHTML)
			{
				if(file_exists($altBody) && is_file($altBody))
					return $this->renderFile($viewFile,$data,true);
			}
		}

		throw new CException(Yii::t('yii','{controller} cannot find the requested email view "{view}".',
			array('{controller}'=>get_class($this), '{view}'=>$view)));
	}

	/**
	 * {@inheritdoc}
	 */
	public function getLayoutFile($layoutName)
	{
		if($layoutName===false)
			return false;

		if(empty($layoutName))
		{
			$module=$this->getModule();
			while($module!==null)
			{
				if($module->layout===false)
					return false;
				if(!empty($module->layout))
					break;
				$module=$module->getParentModule();
			}
			if($module===null)
				$module=Yii::app();
			return $this->resolveViewFile($module->layout,$module->getLayoutPath(),$module->getViewPath());
		}
		else
		{
			if(($module=$this->getModule())===null)
				$module=Yii::app();
			return $this->resolveViewFile($layoutName,$module->getLayoutPath(),$module->getViewPath());
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function getViewPath()
	{
		return Yii::app()->getViewPath().'/'.$this->getId();
	}

	/**
	 * {@inheritdoc}
	 */
	public function getViewFile($viewName)
	{
		$basePath = Yii::app()->getViewPath();
		return $this->resolveViewFile($viewName,$this->getViewPath(),$basePath);
	}

	/**
	 * {@inheritdoc}
	 */
	public function resolveViewFile($viewName,$viewPath,$basePath)
	{
		if(empty($viewName))
			return false;
		if(($renderer=Yii::app()->getViewRenderer())!==null)
			$extension=$renderer->fileExtension;
		else
			$extension='.php';
		if($viewName[0]==='/')
			$viewFile=$basePath.$viewName;
		else if(strpos($viewName,'.'))
			$viewFile=Yii::getPathOfAlias($viewName);
		else
			$viewFile=$viewPath.DIRECTORY_SEPARATOR.$viewName;

		if(is_file($viewFile.$extension))
		{
			return Yii::app()->findLocalizedFile($viewFile.$extension);
		}
		else if($extension!=='.php' && is_file($viewFile.'.php'))
		{
			return Yii::app()->findLocalizedFile($viewFile.'.php');
		}

		return false;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getModule()
	{
		return Yii::app()->controller->module;
	}

	//@todo need to redesign
	public function getId()
	{
		return 'emails';
	}

	public function getUniqueId()
	{
		return $this->getId();
	}

}