<?php

Yii::import('mail.models._base.BaseLetter');

/**
 * @package Mail Module
 * @class Letter
 * @author Revin Roman <xgismox@gmail.com>
 *
 * The main class for working with mail events.
 */
class Letter extends BaseLetter{

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Letter the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		return array(
			array('to, subject', 'required'),
			array('to, subject', 'length', 'max'=>255),
			array('body', 'safe'),
			array('body', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, time_add, time_send, to, to_name, subject, body', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return MailModule
	 */
	public function m(){
		return Yii::app()->getModule('mail');
	}

	/**
	 * Method for rendering views
	 * @static
	 * @param string $path view file path (without ".php")
	 * @param array $data data to be extracted and made available to the view
	 * @return string the rendering result. Null if the rendering result is not required.
	 * @throws LetterException
	 */
	public static function render($path, $data=array()){
		if(!$path){
			throw new LetterException(Yii::t(self::m()->_lng_cat, 'Static property "path" is not defined.'));
		}
		return Yii::app()->getController()->renderFile(Yii::getPathOfAlias(self::m()->path).DIRECTORY_SEPARATOR.$path.'.php', $data, true);
	}

	/**
	 * The method adds a letter to the queue to multiple recipients.
	 * @static
	 * @param array $to who sent the letter (array emails)
	 * @param string $subject subject
	 * @param string|array $body body
	 */
	public static function addStack($to, $subject, $body){
		$to = is_array($to) ? $to : array($to);
		foreach($to as $name=>$e){
			$name = is_int($name) ? false : $name;
			self::add($e, $subject, $body, $name);
		}
	}

	/**
	 * The method adds a letter to the queue.
	 * @static
	 * @param string $to who sent the letter (email)
	 * @param string $subject subject
	 * @param string|array $body body
	 * @param string $to_name who sent the letter (name)
	 * @param Letter $M Letter object
	 * @return bool
	 */
	public static function add($to, $subject, $body, $to_name=false, &$L=false){
		$body = is_array($body) ? self::render($body[0], $body[1]) : $body;

		$L = new self;
		$L->to = $to;
		$L->subject = self::m()->letter_subject_prefix.$subject;
		$L->body = $body;
		$L->to_name = $to_name;
		return $L->save();
	}

	/**
	 * The method sends the current message.
	 */
	public function send(){
		/**
		 * @var $mailer MailSender
		 */
		$mailer = Yii::app()->mail;
		$mailer->Subject = $this->subject;
		$mailer->createMailText($this->body);
		if($mailer->send($this->to, $this->to_name)){
			$this->time_send = time();
			$this->save();
		}
		$mailer->reset();
	}

	/**
	 * @static
	 * The method takes a few emails from the queue and tries to send them.
	 */
	public static function sendQueue(){
		/**
		 * @var Letter[] $LETTERS
		 */
		$criteria = new CDbCriteria();
		$criteria->limit = self::m()->send_per_action;
		$criteria->addColumnCondition(array('time_send'=>0));
		$LETTERS = self::model()->findAll($criteria);

		foreach($LETTERS as $LET){
			$LET->send();
		}
	}

}

/**
 * Class for errors
 */
class LetterException extends CException{}