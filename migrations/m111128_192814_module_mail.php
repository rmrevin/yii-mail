<?php

class m111128_192814_module_mail extends CDbMigration
{
	public function up()
	{
		$opt = 'ENGINE=InnoDB DEFAULT CHARSET=utf8';

		# Create Table 'mail_letters'
		$this->createTable('{{mail_letters}}', array(
			'id' => 'pk',
			'time_add' => 'integer DEFAULT 0',
			'time_send' => 'integer DEFAULT 0',
			'to' => 'string',
			'to_name' => 'string',
			'subject' => 'string',
			'body' => 'text',
		), $opt);
		$this->createIndex('time_send', '{{mail_letters}}', 'time_send');
	}

	public function down()
	{
		$this->dropTable('{{mail_letters}}');
	}
}