<?php

class SenderController extends Controller{

	public $defaultAction = 'send';

	public function actionSend(){
		Letter::sendQueue();
	}
}