<?php

class MailModule extends CWebModule
{
	/**
	 * @var string $path The alias path, with views e-mail messages.
	 */
	public $path = 'application.views._emails';

	/**
	 * @var int number of messages sent over a step
	 */
	public $send_per_action = 5;

	/**
	 * @var string $letter_subject_prefix prefix before subject email
	 */
	public $letter_subject_prefix = '';

	/**
	 * @var string $_lng_cat message category.
	 */
	public $_lng_cat = 'app';

	/**
	 * @var string $defaultController Default controller
	 */
	public $defaultController = 'sender';

	public function init()
	{
		$this->setImport(array(
			'mail.models.*',
			'mail.components.*',
		));
	}
}
